function fetchMessages() {
    const xhr = new XMLHttpRequest();
    xhr.open('GET', '../helpers/fetchMessages.php', true);
    xhr.onreadystatechange = function() {
        if (xhr.readyState === 4 && xhr.status === 200) {
            console.log(xhr.responseText);
            const messages = JSON.parse(xhr.responseText);
            updateChatBox(messages);
        }
    };
    xhr.send();
}


function updateChatBox(messages) {
    const chatBox = document.getElementById('chat-box');
    chatBox.innerHTML = messages.map(message => {
        return `<div class="message"><span class="message-sender">${message.username}</span>  <span class="message-timestamp">${message.timestamp}</span><p>${message.message}</p></div>`;
    }).join('');
}

setInterval(fetchMessages, 2000); 

function sendMessage() {
    const message = document.getElementById('message').value;
    const xhr = new XMLHttpRequest();
    xhr.open('POST', '../helpers/chat.php', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.onreadystatechange = function() {
        if (xhr.readyState === 4 && xhr.status === 200) {
         
            fetchMessages();
            document.getElementById('message').value = '';
        }
    };
    xhr.send(`message=${message}`);
}