$(document).ready(function() {
    $('#listButton').click(function() {
        const url = $('#urlInput').val();
        if (url) {
            $('#listButton').prop('disabled', true);
            $('#spinner').show();
            $('#resultsTable tbody').empty();

            $.ajax({
                url: '../helpers/analysis.php',
                type: 'POST',
                data: { url: url },
                dataType: 'json',
                success: function(response) {
                    $('#spinner').hide();
                    $('#resultsTable').show();
                    $('#listButton').prop('disabled', false);

                    if (response.success) {
                        const words = response.words;
                        const table = $('#resultsTable tbody');
                        let rowCount = 0;
                        for (const word of words) {
                            if (rowCount % 8 === 0) {
                                table.append('<tr>');
                            }
                            table.find('tr').last().append('<td>' + word + '</td>');
                            rowCount++;
                            if (rowCount >= 64) {
                                break;
                            }
                        }
                    } else {
                        console.error('Hiba a szólistázás közben.');
                    }
                },
                error: function(xhr, status, error) {
                    console.error('Hiba a kérés közben: ' + error);
                }
            });
        }
    });
});