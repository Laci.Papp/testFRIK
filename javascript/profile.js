$(document).ready(function() {
   
    $.ajax({
        url: '../helpers/getUserData.php', 
        method: 'GET',
        dataType: 'json',
        success: function(response) {
            if (response.success) {
                const userData = response.data;
                $("#email").val(userData.email); 
                $("#name").val(userData.name);
                $("#marital-status").val(userData.marital_status);
                $("#birthdate").val(userData.birthdate);
                $("#website").val(userData.website);
            } else {
                console.error('Hiba a profil adatok betöltése közben.');
            }
        },
        error: function(xhr, status, error) {
            console.error('Hiba a profil adatok betöltése közben: ' + error);
        }
    });

 
    $("#profile-form").submit(function(event) {
        event.preventDefault();

        const formData = {
            email: $("#email").val(),
            name: $("#name").val(),
            marital_status: $("#marital-status").val(),
            birthdate: $("#birthdate").val(),
            website: $("#website").val()
        };


        $.ajax({
            url: '../helpers/updateUserData.php', 
            method: 'POST',
            data: formData,
            dataType: 'json',
            success: function(response) {
             
                if (response.success) {
                    alert('Profil mentve!');
                } else {
                    console.error('Hiba a profil mentése közben.');
                }
            },
            error: function(xhr, status, error) {
                console.error('Hiba a profil mentése közben: ' + error);
            }
        });
    });
});