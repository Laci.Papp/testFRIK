<?php
require_once '../controllers/userController.php';


$userController = new UserController();


if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $email = $_POST['email'];
    $password = $_POST['password'];
    $user = $userController->login($email, $password);

    if ($user) {
      
        session_start();
        $_SESSION['user_id'] = $user['id'];
        $_SESSION['user_email'] = $user['email'];
       
        $response = ['success' => true];
    } else {
       
        $response = ['success' => false, 'error' => "Hibás e-mail cím vagy jelszó. Kérlek próbáld újra. " . $email . " " . $password];
        
    }
    header('Content-Type: application/json');
    echo json_encode($response);
}
?>
