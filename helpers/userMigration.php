<?php

require_once 'db.php';

class UserMigration {
    public function up() {
      
        $db = DB::getInstance();
        $conn = $db->getConnection();


        $query = "
            CREATE TABLE IF NOT EXISTS users (
                id INT AUTO_INCREMENT PRIMARY KEY,
                email VARCHAR(255) NOT NULL UNIQUE,
                password VARCHAR(255) NOT NULL,
                name VARCHAR(255) NOT NULL,
                marital_status ENUM('egyedülálló', 'házas', 'elvált'),
                birthdate DATE,
                website VARCHAR(255)
            )
        ";

        $stmt = $conn->prepare($query);
        $stmt->execute();
    }
}
?>