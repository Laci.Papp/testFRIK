<?php
require_once '../models/messageModel.php';
require_once '../controllers/chatController.php';
session_start();

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $userId = $_SESSION['user_id'];
    $message = $_POST['message'];
   
    $chatController = new ChatController();
    $chatController->sendMessage($userId, $message);
}
?>