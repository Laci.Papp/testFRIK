<?php
require_once '../controllers/userController.php';
session_start();


if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (isset($_SESSION['user_id'])) {
        $userId = $_SESSION['user_id'];


        $email = $_POST['email'];
        $name = $_POST['name'];
        $maritalStatus = $_POST['marital_status'];
        $birthdate = $_POST['birthdate'];
        $website = $_POST['website'];
     
       /* $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
        $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);
        $maritalStatus = filter_input(INPUT_POST, 'marital_status', FILTER_SANITIZE_STRING);
        $birthdate = filter_input(INPUT_POST, 'birthdate', FILTER_SANITIZE_STRING);
        $website = filter_input(INPUT_POST, 'website', FILTER_VALIDATE_URL);*/

    
        if ($email !== false && $name !== false && $maritalStatus !== false && $website !== false) {
            $userController = new UserController();
            $result = $userController->updateUserProfile($userId, $email, $name, $maritalStatus, $birthdate, $website);
           
            if ($result) {
                $response = [
                    'success' => true,
                    'message' => 'Profil sikeresen frissítve.'
                ];
            } else {
                $response = [
                    'success' => false,
                    'message' => 'Hiba a profil frissítése közben.'
                ];
            }
        } else {
            $response = [
                'success' => false,
                'message' => 'Hiányzó vagy érvénytelen adatok.'
            ];
        }
        $response = [
            'success' => true,
            'message' => 'Profil sikeresen frissítve.'
        ];
    } else {
        $response = [
            'success' => false,
            'message' => 'Nincs bejelentkezett felhasználó.'
        ];
    }
}
header('Content-Type: application/json');
echo json_encode($response);
?>