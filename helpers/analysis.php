<?php
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $url = filter_input(INPUT_POST, 'url', FILTER_VALIDATE_URL);

    if ($url) {
        $content = file_get_contents($url);
        $content = mb_convert_encoding($content, 'UTF-8', 'auto');
        if ($content !== false) {
            $content = strip_tags($content);
            $content = preg_replace('/[^a-zA-Z\s]/', '', $content);
            $words = explode(' ', $content);
            $filteredWords = array_filter($words, function ($word) {
                return stripos($word, 'a') !== false;
            });

            $response = [
                'success' => true,
                'words' => array_slice(array_unique($filteredWords), 0, 64)
            ];
        } else {
            $response = [
                'success' => false,
                'message' => 'Hiba a weboldal tartalmának letöltése közben.'
            ];
        }
    } else {
        $response = [
            'success' => false,
            'message' => 'Érvénytelen URL.'
        ];
    }
} else {
    $response = [
        'success' => false,
        'message' => 'Érvénytelen kérési mód.'
    ];
}

header('Content-Type: application/json; charset=utf-8');
echo json_encode($response);
?>