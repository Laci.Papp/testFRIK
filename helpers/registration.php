<?php
require_once '../controllers/userController.php';


if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    
    $email = $_POST['email'];
    $password = $_POST['password'];
    $name = $_POST['name'];
    $maritalStatus = $_POST['maritalStatus'];
    $birthdate = $_POST['birthdate'];
    $website = $_POST['website'];
   
    $userController = new UserController();

  
    $result = $userController->userRegistration($email, $password, $name, $maritalStatus, $birthdate, $website);

   
    header('Location: ../views/login.php'); 
} else {

    echo "Hibás kérés.";
}
?>