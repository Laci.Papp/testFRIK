<?php
require_once '../controllers/chatController.php';
require_once '../models/messageModel.php';

$chatController = new ChatController();
$messages = $chatController->getAllMessages();

$response = [];

foreach ($messages as $message) {
    $response[] = [
        'username' => $message->getUserName(),
        'message' => $message->getMessage(),
        'timestamp' => $message->getTimestamp(),
    ];
}

header('Content-Type: application/json');
echo json_encode($response);
?>
