<?php

require_once 'db.php';

class ChatMigration {
    public function up() {
        $db = DB::getInstance();
        $conn = $db->getConnection();

        $query = "
            CREATE TABLE IF NOT EXISTS chat_messages (
                id INT AUTO_INCREMENT PRIMARY KEY,
                user_id INT,
                message TEXT,
                timestamp TIMESTAMP
            )
        ";

        $stmt = $conn->prepare($query);
        $stmt->execute();
    }
}
?>