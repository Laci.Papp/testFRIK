<?php
require_once '../controllers/userController.php';
session_start();

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    if (isset($_SESSION['user_id'])) {
        $userId = $_SESSION['user_id'];
        $userController = new UserController();
        $userData = $userController->getUserData($userId);

        if ($userData) {
        
            $response = [
                'success' => true,
                'data' => $userData,
            ];
        } else {
           
            $response = [
                'success' => false,
                'message' => 'Hiba a profiladatok lekérdezése közben.',
            ];
        }
    } else {
      
        $response = [
            'success' => false,
            'message' => 'Nincs bejelentkezett felhasználó.',
        ];
    }
}

header('Content-Type: application/json');
echo json_encode($response);

?>