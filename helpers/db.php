<?php
require_once 'userMigration.php';
require_once 'chatMigration.php';

class DB {
    private static $instance;
    private $connection;

    private $dbhost = "localhost";
    private $dbuser = "root";
    private $dbpass = "";
    private $dbname = "FRIK";

    private function __construct() {
        try {
            $this->connection = new PDO("mysql:host={$this->dbhost};dbname={$this->dbname}", $this->dbuser, $this->dbpass);
            $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            //echo $e->getCode();
            if ($e->getCode() === 1049) {
                try {
                    
                    $this->connection = new PDO("mysql:host={$this->dbhost}", $this->dbuser, $this->dbpass);
                    $this->connection->exec("CREATE DATABASE {$this->dbname}");

                    $userMigration = new UserMigration(); 
                    $userMigration->up();

                    $chatMigration = new ChatMigration(); 
                    $chatMigration->up();       
                   
                
                } catch (PDOException $e) {
                    die("Hiba az adatbázis létrehozásakor: " . $e->getMessage());
                }
            } else {
                die("Hiba a kapcsolódás során: " . $e->getMessage());
            }
        }
    }

    public static function getInstance() {
        if (!self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function getConnection() {
        return $this->connection;
    }
}
?>