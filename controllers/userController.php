<?php
require_once '../helpers/db.php';
require_once '../models/userModel.php';


class UserController {


    public function userRegistration($email, $password, $name, $maritalStatus, $birthdate, $website) {
        $hashedPassword = password_hash($password, PASSWORD_DEFAULT);

        $db = DB::getInstance();
        $conn = $db->getConnection();
    
        $query = "INSERT INTO users (email, password, name, marital_status, birthdate, website) VALUES (:email, :password, :name, :marital_status, :birthdate, :website)";
        $stmt = $conn->prepare($query);
    
    
        $stmt->bindParam(':email', $email);
        $stmt->bindParam(':password', $hashedPassword);
        $stmt->bindParam(':name', $name);
        $stmt->bindParam(':marital_status', $maritalStatus);
        $stmt->bindParam(':birthdate', $birthdate);
        $stmt->bindParam(':website', $website);
    
    
        $stmt->execute();

    }

    public function login($email, $password) {
       
        $db = DB::getInstance();
        $conn = $db->getConnection();
        $stmt = $conn->prepare("SELECT * FROM users WHERE email = :email");
        $stmt->bindParam(':email', $email);
        $stmt->execute();
        $user = $stmt->fetch(PDO::FETCH_ASSOC);
       
        if ($user) {
           
            if (password_verify($password, $user['password'])) {
                
                return $user;
            }
        }

      
        return false;
    }

    public function getUserData($userId) {
        $db = DB::getInstance();
        $conn = $db->getConnection();
        $stmt = $conn->prepare("SELECT * FROM users WHERE id = :id");
        $stmt->bindParam(':id', $userId);
        $stmt->execute();
        $userData = $stmt->fetch(PDO::FETCH_ASSOC);
        return $userData;
    }

    public function updateUserProfile($userId, $email, $name, $maritalStatus, $birthdate, $website) {
        $db = DB::getInstance();
        $conn = $db->getConnection();
    
        
        $user = $this->getUserData($userId);
    
        if ($user) {
           
            $query = "UPDATE users SET email = :email, name = :name, marital_status = :marital_status, birthdate = :birthdate, website = :website WHERE id = :id";
            $stmt = $conn->prepare($query);
    
          
            $stmt->bindParam(':email', $email);
            $stmt->bindParam(':name', $name);
            $stmt->bindParam(':marital_status', $maritalStatus);
            $stmt->bindParam(':birthdate', $birthdate);
            $stmt->bindParam(':website', $website);
            $stmt->bindParam(':id', $userId);
    
       
            $stmt->execute();
    
          
            return true;
        }
    
     
        return false;
    }
    
    
    
}
?>