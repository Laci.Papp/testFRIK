<?php
require_once '../helpers/db.php';
require_once '../models/messageModel.php';


class ChatController {
    public function sendMessage($userId, $message) {
        $db = DB::getInstance();
        $conn = $db->getConnection();

        $query = "INSERT INTO chat_messages (user_id, message, timestamp) VALUES (?, ?, NOW())";
        $stmt = $conn->prepare($query);
        $stmt->execute([$userId, $message]);
    }

    public function getAllMessages() {
        $db = DB::getInstance();
        $conn = $db->getConnection();

       // $query = "SELECT chat_messages.message, users.name, chat_messages.timestamp FROM chat_messages JOIN users ON chat_messages.user_id = users.id ORDER BY chat_messages.timestamp";
        $query = "SELECT chat_messages.id, chat_messages.message, users.name, chat_messages.timestamp
        FROM chat_messages
        JOIN users ON chat_messages.user_id = users.id
        ORDER BY chat_messages.timestamp";
        $stmt = $conn->prepare($query);
        $stmt->execute();

        $messages = array();
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $message = new Message();
          
            $message->setId($row['id']);
            $message->setUserName($row['name']);
            $message->setMessage($row['message']);
            $message->setTimestamp($row['timestamp']);
            $messages[] = $message;
        }

        return $messages;
    }
}
?>