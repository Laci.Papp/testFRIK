<?php
require_once '../helpers/auth.php';
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Chat alkalmazás</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css">
    <style>
        .chat-box {
            border: 1px solid #ccc;
            border-radius: 10px;
            max-height: 300px;
            overflow-y: auto;
            padding: 10px;
            margin-bottom: 20px;
        }
        .message {
            border: 1px solid #ddd;
            border-radius: 5px;
            padding: 10px;
            margin: 5px 0;
        }
        .message-sender {
            font-weight: bold;
        }
        .message-timestamp {
            font-size: 12px;
            color: #888;
        }
        .input-group {
            position: sticky;
            bottom: 0;
            background-color: #fff;
            padding: 10px;
        }
    </style>
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="profile.php">Profil</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="analysis.php">Elemzés</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="chat.php">Chat</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="logout.php">Kijelentkezés</a>
                </li>
            </ul>
        </div>
    </nav>
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="container mt-5">
                <div class="chat-box"  id="chat-box">
                
                </div>
            </div>

            <div class="container mt-3">
                <div class="input-group">
                    <input id="message" type="text" class="form-control" placeholder="Írj egy üzenetet...">
                    <div class="input-group-append">
                        <button  onclick="sendMessage()" class="btn btn-primary">Küldés</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js"></script>
    <script src="../javascript/chat.js"></script>
</body>
</html>