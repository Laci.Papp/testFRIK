<?php
require_once '../helpers/auth.php';
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Felhasználói Profil</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css">
    <style>
        .form-group {
            margin-bottom: 15px;
        }
    </style>
</head>
<body>
    
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="profile.php">Profil</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="analysis.php">Elemzés</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="chat.php">Chat</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="../helpers/logout.php">Kijelentkezés</a>
                </li>
            </ul>
        </div>
    </nav>

    <div class="container mt-5">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <h1>Profil</h1>
                <form id="profile-form">
                    <div class="form-group">
                        <label for="name">Név *</label>
                        <input type="text" class="form-control" id="name" required>
                    </div>
                    <div class="form-group">
                        <label for="email">Email *</label>
                        <input type="email" class="form-control" id="email" required>
                    </div>                  
                    <div class="form-group">
                        <label for="marital-status">Családi Állapot</label>
                        <select class="form-control" id="marital-status">
                            <option value="egyedülálló">Egyedülálló</option>
                            <option value="házas">Házas</option>
                            <option value="elvált">Elvált</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="birthdate">Születési Idő</label>
                        <input type="date" class="form-control" id="birthdate">
                    </div>
                    <div class="form-group">
                        <label for="website">Weboldal</label>
                        <input type="url" class="form-control" id="website">
                    </div>
                    <button type="submit" class="btn btn-primary">Mentés</button>
                </form>
            </div>
        </div>
</div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js"></script>
    <script src="../javascript/profile.js"></script>
</body>
</html>