<?php
class Message {
    private $id;
    private $userId;
    private $message;
    private $userName;
    private $timestamp;

    public function getId() {
        return $this->id;
    }

    public function getUserId() {
        return $this->userId;
    }

    public function getMessage() {
        return $this->message;
    }

    public function getUserName() {
        return $this->userName;
    }

    public function getTimestamp() {
        return $this->timestamp;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setUserId($userId) {
        $this->userId = $userId;
    }

    public function setUserName($userName) {
        $this->userName = $userName;
    }

    public function setMessage($message) {
        $this->message = $message;
    }

    public function setTimestamp($timestamp) {
        $this->timestamp = $timestamp;
    }
}
?>