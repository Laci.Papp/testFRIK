<?php
class User {
    private $id;
    private $email;
    private $password;
    private $name;
    private $maritalStatus;
    private $birthdate;
    private $website;

    public function __construct($id, $email, $password, $name, $maritalStatus, $birthdate, $website) {
        $this->id = $id;
        $this->email = $email;
        $this->password = $password;
        $this->name = $name;
        $this->maritalStatus = $maritalStatus;
        $this->birthdate = $birthdate;
        $this->website = $website;
    }

    public function getId() {
        return $this->id;
    }

    public function getEmail() {
        return $this->email;
    }

    public function setEmail($email) {
        $this->email = $email;
    }

    public function getPassword() {
        return $this->password;
    }

    public function setPassword($password) {
        $this->password = $password;
    }

    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function getMaritalStatus() {
        return $this->maritalStatus;
    }

    public function setMaritalStatus($maritalStatus) {
        $this->maritalStatus = $maritalStatus;
    }

    public function getBirthdate() {
        return $this->birthdate;
    }

    public function setBirthdate($birthdate) {
        $this->birthdate = $birthdate;
    }

    public function getWebsite() {
        return $this->website;
    }

    public function setWebsite($website) {
        $this->website = $website;
    }

}
?>